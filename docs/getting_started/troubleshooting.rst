.. _troubleshooting:

===============
Troubleshooting
===============

The majority of issues new users experience when first using Multi-User can be solved with a few quick checks.

- Run Blender in Administrator mode
- Update the multi-user addon to the latest version
- Make sure to allow Blender through your firewall
    
    .. hint:: Your firewall may have additional settings like Ransomware protection, or you may need to enable both Blender and Python on private and/or public Networks

- Solve problems with your connection quality
- Minimise the use of large textures or file sizes
- Avoid using 'Undo'. Use 'delete' instead

Use the #support channel on the multi-user `discord server <https://discord.gg/aBPvGws>`_ to chat, seek help and contribute.